﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetoresExercicios
{
    class Program
    {
        public void ExercicioDois()
        {
            string frase;
            string vogais = "aeiou";
            Console.Clear();
            Console.WriteLine("Digite uma frase: ");
            frase = Console.ReadLine();
            for (int n = 0; n < frase.Length; n++)
            {
                for (int x = 0; x < vogais.Length; x++)
                {
                    if (Char.ToUpper(frase[n]).Equals(char.ToUpper(vogais[x])))
                    {
                        Console.Write("{0}, ", frase[n]);
                    }
                }
            }
            Console.ReadKey();
        }

        public void ExercicioTres()
        {
            int[] a = { 1, 2, 3, 4, 5 };
            int[] b = { 11, 3, 50, 6, 1, 8, 5, 10 };

            Console.Clear();
            Console.WriteLine("Vetor A: ");
            for (int n = 0; n < a.GetLength(0); n++)
            {
                Console.Write("{0}, ", a[n]);
            }
            Console.WriteLine("\n");
            Console.WriteLine("Vetor B: ");
            for (int n = 0; n < b.GetLength(0); n++)
            {
                Console.Write("{0}, ", b[n]);
            }
            Console.WriteLine("\n");
            Console.WriteLine("Elementos em comum: ");
            for (int n = 0; n < a.GetLength(0); n++)
            {
                for (int m = 0; m < b.GetLength(0); m++)
                {
                    if (a[n] == b[m])
                        Console.Write("{0}, ", a[n]);
                }
            }
            Console.WriteLine();
            Console.ReadKey();



        }

        public void ExercicioQuatro()
        {
            string valBin = "1000";
            int valDec = 0;
            int valor = 0;
            int n = 0;
            int pos;

            Console.Clear();
            Console.Write("Digite uma sequencia de bits: ");
            valBin = Console.ReadLine();
            for (n = valBin.Length - 1, pos = 0; n >= 0; n--, pos++)
            {
                char bit = valBin[n];
                valor = ((int)Math.Pow(2, pos)) * ((int)Char.GetNumericValue(valBin[n]));
                valDec += valor;
            }

            Console.WriteLine("Valor binário : {0}", valBin);
            Console.WriteLine("Valor decimal : {0}", valDec);

            Console.ReadKey();
        }

        public void Menu()
        {
            int opcao = 0;
            bool erro = false;
            do
            {
                Console.Clear();
                Console.WriteLine("Exercícios sobre Vetor\n\n");
                Console.WriteLine("1) Solução do exercício 1");
                Console.WriteLine("2) Solução do exercício 2");
                Console.WriteLine("3) Solução do exercício 3");
                Console.WriteLine("4) Solução do exercício 4");
                Console.WriteLine("0) S A I R\n\n");
                if (erro)
                {
                    Console.WriteLine("Opção inválida!\n");
                }
                erro = false;
                Console.Write("Faça uma escolha: ");
                int.TryParse(Console.ReadLine(), out opcao);
                switch (opcao)
                {
                    case 0:
                        return;
                    case 1:
                        break;
                    case 2:
                        ExercicioDois();
                        break;
                    case 3:
                        ExercicioTres();
                        break;
                    case 4:
                        ExercicioQuatro();
                        break;
                    default:
                        erro = true;
                        break;
                }
                Console.Write("Escolha uma opção: ");

            } while (true);

        }

        static void Main(string[] args)
        {
            Program p = new Program();

            p.Menu();

        }
    }
}
